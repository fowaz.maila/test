<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    public function index(){
        return response()->json([['articles'=>Article::with('authors')->get()], 200]);
    }

    public function find(Request $request){
        $article  = Article::find($request->id);
        if($article)
            return response()->json(['article'=>$article], 200);
        else
            return response()->json(['message'=>'there\'s not such article'], 404);
    }

    public function store(Request $request){
        $validateArticle = Validator::make($request->all(), 
        [
            'title' => ['required', 'string', 'max:255'],
            'publish_date' => ['required', 'date'],
            'content' => ['required'],
            'photo'=>['required']
        ]);

        if($validateArticle->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateArticle->errors()
            ], 401);
        }

        Article::create($request->all());
        return response()->json(['message'=>'Article created succefully'], 201);
    }

    public function update(Request $request){
        $article  = Article::find($request->id);
        if($article){
            $article->update($request->all());
            $article->save();
            return response()->json(['message'=>'article updated successfully'], 200);
        } else
            return response()->json(['message'=>'there\'s not such article'], 404);
    }

    public function delete(Request $request){
        $article  = Article::find($request->id);
        if($article){
            $article->delete();
            return response()->json(['message'=>'article deleted successfully'], 200);
        } else
            return response()->json(['message'=>'there\'s not such article'], 404);
    }

    public function getArticleAuthors(Request $request){
        $article  = Article::find($request->id);
        if($article){
            return response()->json(['authors'=>$article->authors], 200);
        } else
            return response()->json(['message'=>'there\'s not such article'], 404);
    }

    public function addArticleAuthors(Request $request){
        $validateArticle = Validator::make($request->all(), 
        [
            'author_id'=>['required']
        ]);

        if($validateArticle->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateArticle->errors()
            ], 401);
        }

        $article  = Article::find($request->id);
        $author = Author::find($request->author_id);

        if($article){
            if(!$author)
                return response()->json(['message'=>'there\'s not such author'], 404);

            //check if author already attached
            foreach ($article->authors as $x){
                if($x->id==$request->author_id)
                    return response()->json(['message'=>'The author has already been added to the article'], 200);
            }
            
            $article->authors()->attach($author);
            return response()->json(['message'=>'The author has been added to the article successfully'], 200);
        } else
            return response()->json(['message'=>'there\'s not such article'], 404);
    }

    public function removeArticleAuthors(Request $request){
        $validateArticle = Validator::make($request->all(), 
        [
            'author_id'=>['required']
        ]);

        if($validateArticle->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateArticle->errors()
            ], 401);
        }

        $article  = Article::find($request->id);
        $author = Author::find($request->author_id);

        if($article){
            if(!$author)
                return response()->json(['message'=>'there\'s not such author'], 404);

            $article->authors()->detach($author);
            return response()->json(['message'=>'The author has been removed from the article successfully'], 200);
        } else
            return response()->json(['message'=>'there\'s not such article'], 404);
    }
}
