<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    public function index(){
        return response()->json([['authors'=>Author::all()], 200]);
    }

    public function find(Request $request){
        $author  = Author::find($request->id);
        if($author)
            return response()->json(['author'=>$author], 200);
        else
            return response()->json(['message'=>'there\'s not such author'], 404);
    }

    public function store(Request $request){
        $validateAuthor = Validator::make($request->all(), 
        [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:authors'],
            'country' => ['required', 'string', 'max:255'],
        ]);

        if($validateAuthor->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateAuthor->errors()
            ], 401);
        }

        Author::create($request->all());
        return response()->json(['message'=>'Author created succefully'], 201);
    }

    public function update(Request $request){
        $author  = Author::find($request->id);
        if($author){
            $author->update($request->all());
            $author->save();
            return response()->json(['message'=>'author updated successfully'], 200);
        } else
            return response()->json(['message'=>'there\'s not such author'], 404);
    }

    public function delete(Request $request){
        $author  = Author::find($request->id);
        if($author){
            $author->delete();
            return response()->json(['message'=>'author deleted successfully'], 200);
        } else
            return response()->json(['message'=>'there\'s not such author'], 404);
    }
}
