<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'publish_date',
        'content',
        'photo'
    ];

    protected $date = [
        'publish_date'
    ];

    public function authors() {
        return $this->belongsToMany(Author::class);
    }
}
