<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Middleware\checkManager;
use App\Models\Author;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/authors', [AuthorController::class, 'index']);
    Route::get('/articles', [ArticleController::class, 'index']);
    Route::post('/logout', [UserController::class, 'logout']);

    Route::middleware([checkManager::class])->group(function () {
        Route::post('/articles', [ArticleController::class, 'store']);
        Route::get('/articles/{id}', [ArticleController::class, 'find']);
        Route::put('/articles/{id}', [ArticleController::class, 'update']);
        Route::delete('/articles/{id}', [ArticleController::class, 'delete']);

        Route::post('/authors', [AuthorController::class, 'store']);
        Route::get('/authors/{id}', [AuthorController::class, 'find']);
        Route::put('/authors/{id}', [AuthorController::class, 'update']);
        Route::delete('/authors/{id}', [AuthorController::class, 'delete']);

        Route::get('/articleAuthors/{id}', [ArticleController::class, 'getArticleAuthors']);
        Route::post('/addArticleAuthor/{id}', [ArticleController::class, 'addArticleAuthors']);
        Route::post('/removeArticleAuthor/{id}', [ArticleController::class, 'removeArticleAuthors']);
    });
});

Route::post('/register', [UserController::class, 'store']);
Route::post('/login', [UserController::class, 'login']);
